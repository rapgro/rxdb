create table name (
 id integer,
 text varchar(50),
 primary key (id)
);

create unique index name_amount on name (text);


create table value (
 id integer,
 amount varchar(1000),
 primary key (id)
);

create unique index value_amount on value (amount);


create table tag (
 id integer,
 parent integer,
 pos integer,
 name integer,
 primary index (id)
);

create index tag_pos on tag (pos);
create unique index tag_name on tag (pos,name);


create table attribute (
 id integer,
 tag integer,
 name integer,
 value integer,
 primary index(id)
);


